1. Get pycorr code

```
# Sur HOME luke, clone du code pycorr et positonnement sur branche dwl_ENZ_and_rot

mkdir -p ~/PYCORR/version_Pierre_dwl_ENZ_and_rot
cd ~/PYCORR/version_Pierre_dwl_ENZ_and_rot
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:bouep/pycorr.git

cd pycorr

git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/dev_LT_weather_bomb
  remotes/origin/dev_dwl_seis_piel
  remotes/origin/dev_main_bouep
  remotes/origin/dev_weather_bomb
  remotes/origin/dwl_ENZ_and_rot
  remotes/origin/master
  remotes/origin/merge_with_lolo_xcorr

git checkout  remotes/origin/dwl_ENZ_and_rot
Note: checking out 'remotes/origin/dwl_ENZ_and_rot'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:

  git checkout -b <new-branch-name>

HEAD is now at 7c282e5 modified:   v1.0/m_pycorr/m10_get_data.py

git branch -a
* (HEAD detached at origin/dwl_ENZ_and_rot)
  master
  remotes/origin/HEAD -> origin/master
  remotes/origin/dev_LT_weather_bomb
  remotes/origin/dev_dwl_seis_piel
  remotes/origin/dev_main_bouep
  remotes/origin/dev_weather_bomb
  remotes/origin/dwl_ENZ_and_rot
  remotes/origin/master
  remotes/origin/merge_with_lolo_xcorr
```

2. Create a GUIX profile

Use this [manifest file](./manifest_pycorr.scm)

```
> source /applis/site/guix-start.sh

> refresh_guix pycorr

> guix package -m ./manifest_pycorr.scm -p $GUIX_USER_PROFILE_DIR/pycorr

> refresh_guix pycorr
Activate profile  /var/guix/profiles/per-user/lecoinal/pycorr
The following packages are currently installed in /var/guix/profiles/per-user/lecoinal/pycorr:
python-matplotlib  	3.8.2 	out	/gnu/store/j2h6ljw9mb8xpp52zrmg2ql9wn43nwx7-python-matplotlib-3.8.2
python-scikit-learn	1.3.2 	out	/gnu/store/iifal7c9h5filx239c4kzv516yvb6zbb-python-scikit-learn-1.3.2
python-statsmodels 	0.14.0	out	/gnu/store/jx9d5bs5nsa83fxlzjj1dh89a2ylmkk2-python-statsmodels-0.14.0
python-cartopy     	0.22.0	out	/gnu/store/kd6vxkpnp0bl67wq4h9cafl9qnadmj0l-python-cartopy-0.22.0
python-tables      	3.6.1 	out	/gnu/store/fhj82l1jd5h6l0psmsbqkzlzr5c28g7v-python-tables-3.6.1
python-pandas      	1.5.3 	out	/gnu/store/sx4r40c445szqpcm7951f5fzqkg14n1c-python-pandas-1.5.3
python-ipdb        	0.13.9	out	/gnu/store/ij7g2jq0d00k2c00f5y1slsl2db9w831-python-ipdb-0.13.9
python-h5py        	3.8.0 	out	/gnu/store/wpqy9szlyfprd0py5jq3j71hcdprqvj5-python-h5py-3.8.0
python-scipy       	1.12.0	out	/gnu/store/x84r2jjx9dk3s5nmwr4bcmfvyc8wj15f-python-scipy-1.12.0
python-numpy       	1.23.2	out	/gnu/store/474bp59lr4v7g1p2sjg05hh7jmj0p39z-python-numpy-1.23.2
python-obspy       	1.2.2 	out	/gnu/store/j35nszazga3v3zpqffzx5mldlx4992jv-python-obspy-1.2.2
python             	3.10.7	out	/gnu/store/1w5v338qk5m8khcazwclprs3znqp6f7f-python-3.10.7

### puis à la main
##> guix install -p /var/guix/profiles/per-user/lecoinal/pycorr  --with-input=python-pyproj=python-pyproj@3.6.1 python-obspy
## non ca n'ets plus nécessaire, le python-pyproj qui était patché dans la channel gricad a été commenté.
## pareil avec python-ipdb, la version patchée dans channel gricad a été commentée
##lecoinal@luke:~/PYCORR/version_Pierre_dwl_ENZ_and_rot$ guix package -m ./manifest_pycorr.scm -p $GUIX_USER_PROFILE_DIR/pycorr
##guix package: warning: ambiguous package specification `python-ipdb'
##guix package: warning: choosing python-ipdb@0.13.9 from common/python-packages.scm:809:2
##guix package: warning: Consider running 'guix pull' followed by
## non marche pas...
### Ou directement, sans commenter python-obspy du manifest :
##
##> guix package -m ./manifest_pycorr.scm -p $GUIX_USER_PROFILE_DIR/pycorr --with-input=python-pyproj=python-pyproj@3.6.1
```

3. 

```

> which python3
/var/guix/profiles/per-user/lecoinal/pycorr/bin/python3

> echo $PYTHONPATH
> export PYTHONPATH="${PYTHONPATH}:/home/lecoinal/PYCORR/version_Pierre_dwl_ENZ_and_rot/pycorr/v1.0"

> cd pycorr/v1.0/example_noise
> python3 

> python3 00_get_stations.py
> <class 'dict'>
IRIS :  dict   {'II_AAK': {'Network': 'II', 'Station': 'AAK', '
RESIF:  dict   {'G_AIS': {'Network': 'G', 'Station': 'AIS', 'Lo
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
<urlopen error [Errno -2] Name or service not known>
failed to process this request : http://service.iris.edu/irisws/fedcatalog/1/query?net=G%2CII&sta=%2A&loc=%2A&channel=LH%2A&format=text&includeoverlaps=false&nodata=404&includeavailability=True&startbefore=2018-04-16&endafter=2018-05-16


```
