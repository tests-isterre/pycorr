(specifications->manifest
  '("python@3"
    "python-obspy"   ;; pb ? -> resolu, pb d'incompatibilité avec celui patché dans channel gricad
    "python-numpy"
    "python-scipy"
    "python-h5py"
    "python-ipdb"  ;; > marche pas... -> resolu, pb d'incompatibilité avec celui patché dans channel gricad
    "python-pandas"
    "python-tables"  ;; pytables ?
    "python-cartopy"
    "python-statsmodels"
    "python-scikit-learn"
    "python-matplotlib"
;;    "python-scikit-image"
;;    "nlopt-with-python"
;;    "python-memory-profiler"
;;    "python-numba"
;;    "boost"
;;    "openmpi"
;;    "gcc-toolchain"
;;    "gfortran-toolchain"
;;    "zlib"
;;    "hdf5-parallel-openmpi"
;;    "hdf5-parallel-openmpi:fortran"
;;    "time"
;;    "valgrind"
;;    "python-mpi4py"
    ))

